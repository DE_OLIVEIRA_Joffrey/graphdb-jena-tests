package org.graphdb.rdf4j.tutorial;

import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public class Connector {
    private static Logger logger = LoggerFactory.getLogger(Connector.class);
    private static final Marker MARKER = MarkerFactory.getMarker("");
    private static final String GRAPHDB_SERVER = "http://localhost:7200/";
    private static final String REPOSITORY_ID = "test";

    private static String strInsert =
            "INSERT DATA {"
                    + "<http://dbpedia.org/resource/FS-A1WSX> <http://www.w3.org/2000/01/rdf-schema#rdfs:comment> \"Test comment\" ."
                    + "}";


    private static RepositoryConnection getRepositoryConnection() {
        Repository repository = new HTTPRepository(GRAPHDB_SERVER, REPOSITORY_ID);
        repository.initialize();
        return repository.getConnection();
    }

    private static void insert(RepositoryConnection repositoryConnection) {

        repositoryConnection.begin();
        Update updateOperation = repositoryConnection.prepareUpdate(QueryLanguage.SPARQL, strInsert);
        updateOperation.execute();

        try {
            repositoryConnection.commit();
        } catch (Exception e) {
            if (repositoryConnection.isActive())
                repositoryConnection.rollback();
        }
    }

    public static void main(String[] args) {
        RepositoryConnection repositoryConnection = null;
        try {
            repositoryConnection = getRepositoryConnection();
            insert(repositoryConnection);

        } catch (Throwable t) {
            logger.error(MARKER, t.getMessage(), t);
        } finally {
            repositoryConnection.close();
        }
    }
}